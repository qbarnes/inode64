#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <exception>
#include <system_error>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include "file_desc.h"
#include "file_stat.h"

namespace bpo = boost::program_options;

std::string FilePathPrefix;

class usage_error : public std::runtime_error {
public:
	explicit usage_error(const std::string& what_arg) :
			     runtime_error(what_arg) {};
};

void
parse_options(int argc, const char** argv) throw(usage_error)
{
	try {
		bpo::options_description desc("Options");
		desc.add_options()
			("help,h", "show help message")
			(",p", bpo::value<std::string>(&FilePathPrefix)->
						default_value("dir000/fn-"),
				"filepath prefix");

		bpo::positional_options_description p;
		bpo::variables_map vm;

		bpo::store(bpo::command_line_parser(argc, argv).
			options(desc).positional(p).run(), vm);
		bpo::notify(vm);

		if (vm.count("help")) {
			std::stringstream	usage;

			usage << argv[0] << " [options]\n" << desc;
			throw(usage_error(usage.str()));
		}

	} catch (usage_error&) {
		throw;
	} catch (std::exception& e) {
		throw(usage_error(std::string(e.what()) +
			"\n--help for help"));
	}
}


int
main(int argc, const char** argv)
{
	int	ret = 0;

	try {
		parse_options(argc, argv);

		for (unsigned long i = 0; ret == 0; ++i) {
			std::stringstream ss;

			ss << FilePathPrefix << boost::format("%1$020d") % i;

			auto fn = ss.str();
			auto fnstat = get_file_stat(file_desc(fn));

			if (fnstat->st_ino > 0xffffffff) {
				std::cout << "file = " << fn << std::endl;
				break;
			}
		}
	} catch (usage_error& e) {
		std::cerr << "Usage: " << e.what() << std::endl;
		ret = 1;
	} catch (std::exception& e) {
		std::cerr << "Fatal error: " << e.what() << '.' << std::endl;
		ret = 1;
	} catch (...) {
		std::cerr << "Uncaught error." << std::endl;
		ret = 2;
	}

	return ret;
}
