CXX = /opt/rh/devtoolset-4/root/usr/bin/g++
CXXOPTFLAGS = -O
CXXFLAGS += -Wall -g -std=c++14 $(CXXOPTFLAGS)
LDLIBS += -lboost_program_options

all: inode64

inode64: file_desc.o file_stat.o

file_stat.o: file_stat.h

file_desc.o: file_desc.h

clean clobber distclean:
	rm -f -- inode64 file_desc.o file_stat.o
