#ifndef file_desc_h
#define file_desc_h

#include <unistd.h>
#include <fcntl.h>
#include <memory>

const auto file_desc_def_flags = O_RDWR|O_CREAT;
const auto file_desc_def_mode  = S_IRUSR|S_IWUSR;


class file_desc {
	std::shared_ptr<int>	fdp;

public:
	explicit file_desc(int fd = -1) : fdp(std::make_shared<int>())
			   { *fdp = fd; }
	explicit file_desc(const char* pathname,
			   int    flags = file_desc_def_flags,
			   mode_t mode  = file_desc_def_mode);
	explicit file_desc(const std::string& pathname,
			   int    flags = file_desc_def_flags,
			   mode_t mode  = file_desc_def_mode);

	~file_desc() { _close(); }

	file_desc& operator=(const file_desc& rhs);

	operator int() const { return *fdp; }

	void open(const char* pathname,
		  int    flags = file_desc_def_flags,
		  mode_t mode  = file_desc_def_mode);
	void open(const std::string& pathstr,
		  int    flags = file_desc_def_flags,
		  mode_t mode  = file_desc_def_mode);

	int _close();
	void close();
};

#endif
