#ifndef file_stat_h
#define file_stat_h

#include <sys/stat.h>
#include <memory>
#include "file_desc.h"

typedef std::unique_ptr<struct stat> file_stat;

file_stat get_file_stat(int fd);
file_stat get_file_stat(const file_desc& fd);

#endif
